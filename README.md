[In development]
SymbolicNumber is an alternative to double that can represent numbers in Q[sqrt(2)] with perfect precision. We need this because we will have to deal with many sqrt(2) and don't want rounding errors.

[Planned]
SymbolicComplex contains complex numbers with components of the type SymbolicNumber.

[Planned]
Gates contain a 2^n x 2^n hermitian matrix and a way to multiply it with a QuantumState. Tensor products must probably be implemented. Indices start at 1!

[Planned]
QuantumState saves a number of possibly interating qubits. Basically a vactor with 2^n components. Size is allocated at construction and cannot be changed. Indices start at 1!

















