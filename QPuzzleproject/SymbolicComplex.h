#pragma once
#include "SymbolicNumber.h"

class SymbolicComplex {
private:
	SymbolicNumber re;
	SymbolicNumber im;
public:
	SymbolicComplex(double);
	SymbolicComplex(double, double);
	SymbolicComplex(SymbolicNumber);
	SymbolicComplex(SymbolicNumber, SymbolicNumber);
	const SymbolicComplex operator+(const SymbolicComplex&) const;
	const SymbolicComplex operator-(const SymbolicComplex&) const;
	const SymbolicComplex operator*(const SymbolicComplex&) const;

};

