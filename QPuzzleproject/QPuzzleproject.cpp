#include "pch.h"
#include "SymbolicNumber.h"
#include <iostream>

using std::cout;
using std::endl;

int main()
{
	SymbolicNumber x(0, { 0 }, { 0, 0, 1 });
	SymbolicNumber y(0, { 0 }, { 0, 1 });

	double a = std::pow(2, -2.5);
	double b = std::pow(2, -1.5);

	cout << "Standard:" << endl;
	cout << a << "*" << b << " = " << a*b << endl;
	cout << "Symbolic:" << endl;
	cout << x << "*" << y << " = " << x*y << endl;
}

