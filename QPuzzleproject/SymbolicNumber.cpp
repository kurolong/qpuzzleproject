#include "pch.h"
#include "SymbolicNumber.h"
#include <algorithm>
#include <cmath>
#include <iostream>

SymbolicNumber::SymbolicNumber(double x){
	positivePart = vector<double>(0);
	negativePart = vector<double>(0);
	rationalPart = x;
}

SymbolicNumber::SymbolicNumber(double x, vector<double> p) {
	positivePart = p;
	negativePart = vector<double>(0);
	rationalPart = x;
}

SymbolicNumber::SymbolicNumber(double x, vector<double> p, vector<double> n) {
	positivePart = p;
	negativePart = n;
	rationalPart = x;
}

const SymbolicNumber SymbolicNumber::operator+(const SymbolicNumber& other) const {
	SymbolicNumber result = *this;
	result.rationalPart += other.rationalPart;
	result.positivePart.resize(std::max(positivePart.size(), other.positivePart.size()));
	result.negativePart.resize(std::max(negativePart.size(), other.negativePart.size()));
	
	for (int i = 0; i < result.positivePart.size(); i++) {
		result.positivePart[i] = (i >= positivePart.size() ? 0 : positivePart[i]) + (i >= other.positivePart.size() ? 0 : other.positivePart[i]);
	}
	for (int i = 0; i < result.negativePart.size(); i++) {
		result.negativePart[i] = (i >= negativePart.size() ? 0 : negativePart[i]) + (i >= other.negativePart.size() ? 0 : other.negativePart[i]);
	}

	return result;
}

const SymbolicNumber SymbolicNumber::operator-(const SymbolicNumber& other) const {
	
	SymbolicNumber result = *this;
	result.rationalPart -= other.rationalPart;
	result.positivePart.resize(std::max(positivePart.size(), other.positivePart.size()));
	result.negativePart.resize(std::max(negativePart.size(), other.negativePart.size()));

	for (int i = 0; i < result.positivePart.size(); i++) {
		result.positivePart[i] = (i >= positivePart.size() ? 0 : positivePart[i]) - (i >= other.positivePart.size() ? 0 : other.positivePart[i]);
	}
	for (int i = 0; i < result.negativePart.size(); i++) {
		result.negativePart[i] = (i >= negativePart.size() ? 0 : negativePart[i]) - (i >= other.negativePart.size() ? 0 : other.negativePart[i]);
	}

	return result;
}

const SymbolicNumber SymbolicNumber::operator*(const SymbolicNumber& other) const {
	SymbolicNumber result = SymbolicNumber(0);
	result.positivePart.resize(std::max(positivePart.size(), other.positivePart.size()));
	result.negativePart.resize(std::max(negativePart.size(), other.negativePart.size()));

	for (int i = 0; i < positivePart.size(); i++) {
		for (int j = 0; j < other.positivePart.size(); j++) {
			result.rationalPart += positivePart[i] * other.positivePart[i] * std::pow(2, i + j + 1);
		}
		for (int k = 0; k < other.negativePart.size(); k++) {
			result.rationalPart += positivePart[i] * other.negativePart[i] * std::pow(2, i - k);
		}
	}

	for (int i = 0; i < negativePart.size(); i++) {
		for (int j = 0; j < other.positivePart.size(); j++) {
			result.rationalPart += negativePart[i] * other.positivePart[j] * std::pow(2, -i + j);
		}
		for (int k = 0; k < other.negativePart.size(); k++) {
			result.rationalPart += negativePart[i] * other.negativePart[k] * std::pow(2, -i - k - 1);
		}
	}

	//TODO: handle non-rational parts

	for (int i = 0; i < result.positivePart.size(); i++) {
		result.positivePart[i] = other.rationalPart * (i >= positivePart.size() ? 0 : positivePart[i]);
		result.positivePart[i] += rationalPart * (i >= other.positivePart.size() ? 0 : other.positivePart[i]);
	}

	for (int i = 0; i < result.negativePart.size(); i++) {
		result.negativePart[i] = other.rationalPart * (i >= negativePart.size() ? 0 : negativePart[i]);
		result.negativePart[i] += rationalPart * (i >= other.negativePart.size() ? 0 : other.negativePart[i]);
	}

	return result;
}

SymbolicNumber& SymbolicNumber::operator+=(const SymbolicNumber &) {
	//DUMMY
	return *this;
}

SymbolicNumber& SymbolicNumber::operator-=(const SymbolicNumber &) {
	//DUMMY
	return *this;
}

SymbolicNumber& SymbolicNumber::operator*=(const SymbolicNumber &) {
	//DUMMY
	return *this;
}

SymbolicNumber::operator double() const {
	double result = rationalPart;
	
	for (int i = 0; i < positivePart.size(); i++) {
		result += positivePart[i] * std::pow(2, i + 0.5);
	}
	for (int i = 0; i < negativePart.size(); i++) {
		result += negativePart[i] * std::pow(2, -i - 0.5);
	}

	return result;
}
