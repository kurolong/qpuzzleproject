#include "pch.h"
#include "SymbolicComplex.h"


SymbolicComplex::SymbolicComplex(double x) : re(x), im(0) {}

SymbolicComplex::SymbolicComplex(double x, double y) : re(x), im(y) {}

SymbolicComplex::SymbolicComplex(SymbolicNumber x) : re(x), im(0) {}

SymbolicComplex::SymbolicComplex(SymbolicNumber x, SymbolicNumber y) : re(x), im(y) {}

const SymbolicComplex SymbolicComplex::operator+(const SymbolicComplex & x) const
{
	return SymbolicComplex(re + x.re, im + x.im);
}

const SymbolicComplex SymbolicComplex::operator-(const SymbolicComplex & x) const
{
	return SymbolicComplex(re - x.re, im - x.im);
}

const SymbolicComplex SymbolicComplex::operator*(const SymbolicComplex & x) const
{
	return SymbolicComplex(re * x.re - im * x.im, re * x.im + im * x.re);
}
