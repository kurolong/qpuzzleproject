#pragma once
#include<vector>

using std::vector;

class SymbolicNumber
{
private:
	vector<double> positivePart;
	vector<double> negativePart;
	double rationalPart;
public:
	SymbolicNumber(double);
	SymbolicNumber(double, vector<double>);
	SymbolicNumber(double, vector<double>, vector<double>);
	const SymbolicNumber operator+(const SymbolicNumber&) const;
	const SymbolicNumber operator-(const SymbolicNumber&) const;
	const SymbolicNumber operator*(const SymbolicNumber&) const;
	SymbolicNumber& operator+=(const SymbolicNumber&); //TODO
	SymbolicNumber& operator-=(const SymbolicNumber&); //TODO
	SymbolicNumber& operator*=(const SymbolicNumber&); //TODO
	operator double() const;
};

